<?php

/**
 * FAE GraphQL Mutations
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @license LGPLv3
 * @copyright 2020 Callum Smith
 */

namespace FAE\graphql;

use FAE\fae\fae;
use FAE\graphql\mutation\exception;
use FAE\graphql\mutation\mutation;
use FAE\rest\events\restSchemaRenderPreHook;
use JsonSerializable;
use stdClass;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class graphQL implements JsonSerializable, EventSubscriberInterface
{

  // @var RouteCollection
  static $_routes;

  // @var object
  protected $options;

  public function __construct()
  {
    $this->options = (array) fae::loadOptions('graphql');
  }

  /**
   * Event subscriber function
   *
   * @return array
   */
  public static function getSubscribedEvents(): array
  {
    return [
      restSchemaRenderPreHook::NAME => 'restSchemaRender',
    ];
  }

  public function restSchemaRender(restSchemaRenderPreHook $event)
  {
    $event->setSchema((object) array_merge((array) $event->getSchema(), (array) json_decode(json_encode($this))));
  }

  /**
   * Static route loader called by routeloader.php
   *
   * @return RouteCollection
   */
  static function routes(): RouteCollection
  {
    $self = new self();
    return $self->loadRoutes();
  }

  /**
   * Load routes for the defined mutations
   *
   * @return RouteCollection
   */
  public function loadRoutes(): RouteCollection
  {
    global $config;

    if (!self::$_routes) {
      self::$_routes = new RouteCollection();
    }

    // Load mutations
    if (array_key_exists('mutations', $this->options) && is_array($this->options['mutations'])) {
      foreach ($this->options['mutations'] as $mutation) {
        $instance = new $mutation();
        $route = new Route(
          '/' . $instance->getName(),
          ['_controller' => '\\FAE\\rest\\rest', '_action' => $mutation],
          [],
          [],
          '',
          [],
          [$instance->getMethod()]
        );
        self::$_routes->add("graphql-mutation-{$instance->getName()}", $route);
      }
    }

    // Load mutations
    if (array_key_exists('queries', $this->options) && is_array($this->options['queries'])) {
      foreach ($this->options['queries'] as $query) {
        $instance = new $query();
        $route = new Route(
          '/' . $instance->getName(),
          ['_controller' => '\\FAE\\rest\\rest', '_action' => $query],
          [],
          [],
          '',
          [],
          [$instance->getMethod()]
        );
        self::$_routes->add("graphql-mutation-{$instance->getName()}", $route);
      }
    }

    self::$_routes->addPrefix("/api/{$config->apiVersion}");
    self::$_routes->setSchemes(['https', 'http']);

    return self::$_routes;
  }

  public function jsonSerialize(): object
  {
    $schema = new stdClass;
    if (array_key_exists('mutations', $this->options) && is_array($this->options['mutations'])) {
      $schema->mutations = new stdClass;
      $schema->mutations->items = new stdClass;
      foreach ($this->options['mutations'] as $mutation) {
        $instance = new $mutation();
        $schema->mutations->items->{$instance->getName()} = $instance;
      }
    }
    if (array_key_exists('queries', $this->options) && is_array($this->options['queries'])) {
      $schema->queries = new stdClass;
      $schema->queries->items = new stdClass;
      foreach ($this->options['queries'] as $query) {
        $instance = new $query();
        $schema->queries->items->{$instance->getName()} = $instance;
      }
    }
    return $schema;
  }
}
