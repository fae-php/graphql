<?php

/**
 * FAE GraphQL Mutations
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @license LGPLv3
 * @copyright 2020 Callum Smith
 */

namespace FAE\graphql\mutation;

use RuntimeException;

/**
 * @deprecated v1.4.0 Update mutation exception reference
 */

class exception extends RuntimeException
{
}


