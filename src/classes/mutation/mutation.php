<?php

/**
 * FAE GraphQL Mutations
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @license LGPLv3
 * @copyright 2020 Callum Smith
 */

namespace FAE\graphql\mutation;

use FAE\graphql\endpointAbstract;
use FAE\graphql\type\input;
use Symfony\Component\HttpFoundation\Request;

abstract class mutation extends endpointAbstract
{

  // @var string Method that the endpoint can be called on
  protected $method = "POST";

  /**
   * Validate the mutation request, this function is usually called within the {@see run()} function
   *
   * @todo Add type validation on parameter data
   * @param array $parameters
   * @throws \FAE\graphql\mutation\columnException
   * @return bool Returns true on succesful validaiton, throws {@see \FAE\graphql\mutation\columnException} on error
   */
  protected function validateRequest(array $parameters): bool
  {
    $input = $this->getInput();
    foreach ($input->getColumns() as $column) {
      if ($column->getRequired() && !array_key_exists($column->getName(), $parameters)) {
        throw new columnException($column);
      }
    }
    return true;
  }

  public function jsonSerialize(): array
  {
    global $config;
    return [
      "mutation"    => $this->name,
      "input"       => $this->getInput(),
      "type"        => $this->getReturnType(),
      "method"      => $this->method,
      "description" => $this->description,
      "endpoint"    => BASE_URL . "/api/{$config->apiVersion}/{$this->name}",
    ];
  }

  public function action(): object
  {
    $data = $this->run($this->request);
    return $this->formatData($data);
  }

  abstract function getInput(): ?input;
  abstract function getReturnType();
  abstract function run(Request $request);
}
