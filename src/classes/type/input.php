<?php

/**
 * FAE GraphQL Mutations
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @license LGPLv3
 * @copyright 2020 Callum Smith
 */

namespace FAE\graphql\type;

use FAE\schema\types\model;

abstract class input extends model
{
  private $isArray = false;

  /**
   * Get whether the input is an array type or not
   *
   * @return bool
   */
  public function getIsArray(): bool
  {
    return $this->isArray;
  }

  /**
   * Set whether the input is an array or not
   *
   * @param boolean $isArray
   * @return void
   */
  public function isArray(bool $isArray): void
  {
    $this->isArray = $isArray;
  }

  /**
   * Get a representation of the input type
   *
   * @return object|array Array or object representing input type depending on the isArray variable
   */
  public function jsonSerialize() : mixed
  {
    $return = parent::jsonSerialize();
    return $this->isArray ? [$return] : $return;
  }
}
