<?php

/**
 * FAE GraphQL Mutations
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @license LGPLv3
 * @copyright 2020 Callum Smith
 */

namespace FAE\graphql\type;

use FAE\schema\types\column as schemaColumn;

class column extends schemaColumn
{
}
