<?php

/**
 * FAE GraphQL Mutations
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @license LGPLv3
 * @copyright 2020 Callum Smith
 */

namespace FAE\graphql\mutation;

use FAE\schema\types\column;

class columnException extends exception
{
  public function __construct(column $column)
  {
    $this->message = "Missing " . ($column->getRequired() ? "required " : "") . "property '{$column->getName()}' from request";
  }
}
