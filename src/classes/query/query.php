<?php

/**
 * FAE GraphQL Queries
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @license LGPLv3
 * @copyright 2020 Callum Smith
 */

namespace FAE\graphql\query;

use FAE\graphql\endpointAbstract;
use FAE\schema\types\model;

abstract class query extends endpointAbstract
{

  public function jsonSerialize(): array
  {
    global $config;
    return array_merge([
      "method"      => $this->method,
      "endpoint"    => BASE_URL . "/api/{$config->apiVersion}/{$this->name}",
    ], json_decode(json_encode($this->getModel()), true));
  }
  
  abstract function getModel(): model;
}