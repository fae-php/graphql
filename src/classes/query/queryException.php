<?php

/**
 * FAE GraphQL Queries
 * 
 * @author Callum Smith <callumsmith@me.com>  
 * @author Fiona Sanderson <fionasanderson@protonmail.com>
 * @license LGPLv3
 * @copyright 2020 Callum Smith Fiona Sanderson
 */

namespace FAE\graphql\query;

use RuntimeException;

class queryException extends RuntimeException
{
}