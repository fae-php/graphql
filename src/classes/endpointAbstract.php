<?php

/**
 * FAE GraphQL Mutations
 * 
 * @author Callum Smith <callumsmith@me.com>
 * @license LGPLv3
 * @copyright 2020 Callum Smith
 */

namespace FAE\graphql;

use FAE\graphql\type\input;
use FAE\rest\restActionAbstract;
use JsonSerializable;
use Symfony\Component\HttpFoundation\Request;

abstract class endpointAbstract extends restActionAbstract implements JsonSerializable
{
  // @var string name of the endpoint
  protected $name;

  // @var string Description for the endpoint
  protected $description;

  // @var string Method that the endpoint can be called on
  protected $method = "GET";

  // @var permAbstract The query handler
  protected $dataInstance;

  public function __construct()
  {
    if (!$this->name) {
      $this->name = get_called_class();
    }
  }

  public function getName(): string
  {
    return $this->name;
  }

  public function getMethod(): string
  {
    return $this->method;
  }

  public static function method(): string
  {
    $self = new self();
    return $self->getMethod();
  }
}